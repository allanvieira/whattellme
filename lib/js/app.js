var dateParse = d3.time.format('%Y-%m-%d');
var dateFormat = d3.time.format("%d/%m/%Y");
var months = ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Agosto','Setembro','Outubro','Novembro','Dezembro'];

var locale = d3.locale({
  "decimal": ",",
  "thousands": ".",
  "grouping": [3],
  "currency": ["R$", ""],
  "dateTime": "%a %b %e %X %Y",
  "date": "%d/%m/%Y",
  "time": "%H:%M:%S",
  "periods": ["AM", "PM"],
  "days": ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
  "shortDays": ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
  "months": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
  "shortMonths": ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"]
});
d3.time.format = locale.timeFormat;
var numberFormatDecimal = d3.format(',.2f');
var numberFormat = d3.format(',');
var changeDecimal = function (v){
  return v.replace(/\./g,';').replace(/,/g,'.').replace(/;/g,',');  
}

var dashboard = function(){
  this.init = function(container){
    this._group = container;
    this._container = $('#'+container)[0];
    this.getData();
  }

  this.getData = function(){
    $.getJSON("core/getdata.php", function(data) {
      this._data = data;
      this.initCrossFilter();
    }.bind(this));
  }

  this.initCrossFilter = function(){
    this._data.forEach(function(d){
      d.indexed_at = dateParse.parse(d.indexed_at);
      d.month = d.indexed_at.getMonth();
      d.tokens_count = JSON.parse(d.tokens_count);
      d.bigrams = JSON.parse(d.bigrams.replace(/{/g,'[').replace(/}/g,']'));
      d.word_felligns = JSON.parse(d.word_felligns);
      d.avg_alert = parseFloat(d.avg_alert)
      d.avg_valence = parseFloat(d.avg_valence)
      d.alert = Math.round(d.avg_alert)
      d.valence = Math.round(d.avg_valence)
      d.fellign = d.avg_alert > 0 ? 'Sim': 'Não';
      d.all = 0;
    });
    this._ndx = crossfilter(this._data);
    this._allMap = this._ndx.dimension(function(d){return d.all;});
    this._allReduceCount = this._allMap.group().reduceCount();
    this._total = this._allReduceCount.all()[0].value
    this.setCharts();
  }
  this.setCharts = function(){
    this._charts = {};

    // Quantidade Resultado por Dia
    var chartDateCount = {};
    chartDateCount.dom = dc.lineChart(('#'+this._group+' .chart-date-count'),this._group);
    chartDateCount.map = this._ndx.dimension(function(d){return d.indexed_at;});
    chartDateCount.reduce = chartDateCount.map.group().reduceCount();
    chartDateCount.reduce = {
      all : function(){
        return chartDateCount.map.group().reduceCount().all().filter(function(d){
          return d.value != 0;
        });
      }.bind(chartDateCount)
    };
    chartDateCount.dom
  		.height(250)
  		.dimension(chartDateCount.map)
  		.group(chartDateCount.reduce,"Total")
  		.renderArea(true)
  		.x(d3.time.scale().domain([chartDateCount.map.bottom(1)[0].data,chartDateCount.map.top(1)[0].data]))
      .yAxisLabel("Resultados por Dia")
  		.elasticX(true)
  		.elasticY(true)
  		.brushOn(false)
      .mouseZoomable(false)
  		.legend(dc.legend().x(90).y(10).itemHeight(13).gap(5))
  		.renderHorizontalGridLines(true)
  		.renderVerticalGridLines(true)
  		.renderTitle(true)
  		._rangeBandPadding(1)
      .title(function(d){return dateFormat(d.key) + ' : ' + changeDecimal(numberFormat(d.value)) + ' resultados ( ' + changeDecimal(numberFormatDecimal((d.value/this._total*100))) + '% )' }.bind(this))
  		.margins({ top: 20, left: 50, right: 10, bottom: 50 })
  		.renderlet(function (chart) {chart.selectAll("g.x text").attr("dx", "-30").attr("dy", "2").attr("transform", "rotate(-35)");});
      /*.on('pretransition', function(chart) {
        chart.selectAll("circle.dot").on("click", function (d) {
            console.log('click');
            chart.filter(null)
                .filter(d.data.key)
                .redrawGroup();
        })*/    
    
    chartDateCount.dom.xAxis().tickFormat(function(v){return dateFormat(v)});
    this._charts['chartDateCount'] = chartDateCount;

    // Quantidade de Resultado por Mês
    var charMonthCount = {};
    charMonthCount.dom = dc.rowChart(('#'+this._group+' .chart-month-count'),this._group);
    charMonthCount.map = this._ndx.dimension(function(d){return d.month;});
    charMonthCount.reduce = charMonthCount.map.group().reduceCount();
    charMonthCount.dom
      .height(250)
      .dimension(charMonthCount.map)
      .group(charMonthCount.reduce)
      .ordering(function(d){ return d.key;})
      .label(function(d) { return months[d.key];})
      .title(function(d){ return months[d.key] + " : " + changeDecimal(numberFormat(d.value)) + " Resultados ( " + changeDecimal(numberFormatDecimal((d.value/this._total*100))) + '% )';}.bind(this) )
      .on("filtered", function(chart, filter){
        this.onFiltered()        
      }.bind(this))
       .margins({ top: 0, left: 10, right: 0, bottom: 20 })
      .elasticX(true)
      .xAxis().ticks(4);
    this._charts['charMonthCount'] = charMonthCount;

    // Quantidade de Resultado por Site
    var charDomainCount = {};
    charDomainCount.dom = dc.rowChart(('#'+this._group+' .chart-domain-count'),this._group);
    charDomainCount.map = this._ndx.dimension(function(d){return d.domain;});
    charDomainCount.reduce = charDomainCount.map.group().reduceCount();
    charDomainCount.dom
      .height(250)
      .dimension(charDomainCount.map)
      .group(charDomainCount.reduce)
      .ordering(function(d){ return d.value;})
      .rowsCap(12)
      .othersGrouper(false)
      .label(function(d) { return d.key;})
      .title(function(d){ return d.key + " : " + d.value + " Resultados ( " + changeDecimal(numberFormatDecimal((d.value/this._total*100))) + '% )';}.bind(this) )
      .on("filtered", function(chart, filter){
        this.onFiltered()        
      }.bind(this))
       .margins({ top: 0, left: 10, right: 0, bottom: 20 })
      .elasticX(true)
      .xAxis().ticks(4);
    this._charts['charDomainCount'] = charDomainCount;

    // Quantidade de Resultado por Alerta
    var charAlertCount = {};
    charAlertCount.dom = dc.barChart(('#'+this._group+' .chart-alert-count'),this._group);
    charAlertCount.map = this._ndx.dimension(function(d){if(d.avg_alert > 0){return d.alert}else{return -1}});
    charAlertCount.reduce = charAlertCount.map.group().reduceCount();
    charAlertCount.dom
      .height(90)
      .x(d3.scale.linear().domain([0,9]))
      .brushOn(false)
      .dimension(charAlertCount.map)
      .group(charAlertCount.reduce)
      // .ordering(function(d){ return d.key;})            
      .label(function(d) { return d.key;})
      .title(function(d){ return d.key + " : " + d.value + " Resultados ( " + changeDecimal(numberFormatDecimal((d.value/this._total*100))) + '% )';}.bind(this) )
      .on("filtered", function(chart, filter){
        this.onFiltered()        
      }.bind(this))
       .margins({ top: 5, left: 45, right: 10, bottom: 20 });
    this._charts['charAlertCount'] = charAlertCount;

    // Quantidade de Resultado por Alerta
    var charValenceCount = {};
    charValenceCount.dom = dc.barChart(('#'+this._group+' .chart-valence-count'),this._group);
    charValenceCount.map = this._ndx.dimension(function(d){if(d.avg_valence > 0){return d.valence}else{return -1}});
    charValenceCount.reduce = charValenceCount.map.group().reduceCount();
    charValenceCount.dom
      .height(90)
      .x(d3.scale.linear().domain([0,9]))
      .brushOn(false)
      .elasticY(true)
      .dimension(charValenceCount.map)      
      .group(charValenceCount.reduce)
      // .ordering(function(d){ return d.key;})            
      .label(function(d) { return d.key;})
      .title(function(d){ return d.key + " : " + d.value + " Resultados ( " + changeDecimal(numberFormatDecimal((d.value/this._total*100))) + '% )';}.bind(this) )
      .on("filtered", function(chart, filter){
        this.onFiltered()        
      }.bind(this))
       .margins({ top: 5, left: 45, right: 10, bottom: 20 });
    this._charts['charValenceCount'] = charValenceCount;



    // Quantidade de Resultado por Sentimento
    var chartFellignCount = {};
    chartFellignCount.dom = dc.pieChart(('#'+this._group+' .chart-fellign-count'),this._group);
    chartFellignCount.map = this._ndx.dimension(function(d){return d.fellign;});
    chartFellignCount.reduce = chartFellignCount.map.group().reduceCount();
    chartFellignCount.dom
      .height(100)
      .dimension(chartFellignCount.map)
      .group(chartFellignCount.reduce)            
      .title(function(d){ return d.key + " : " + changeDecimal(numberFormat(d.value)) + " Resultados ( " + changeDecimal(numberFormatDecimal((d.value/this._total*100))) + '% )';}.bind(this) );
    this._charts['chartFellignCount'] = chartFellignCount;

    // Total de Resultado
    var charTotalCount = {};
    charTotalCount.dom = dc.numberDisplay(('#'+this._group+' #total'),this._group);    
    charTotalCount.reduce = this._allMap.group().reduceCount();
    charTotalCount.dom          
      .formatNumber(function(v){return changeDecimal(numberFormat(v))})
      .group(charTotalCount.reduce);
    this._charts['charTotalCount'] = charTotalCount;

    // Total de Dominios
    var charTotalDomainsCount = {};
    charTotalDomainsCount.dom = dc.numberDisplay(('#'+this._group+' #totalDomains'),this._group);        
    charTotalDomainsCount.reduce = this._allMap.group().reduce(
      function add(p, d){
        if (d.domain in p.domains){
          p.domains[d.domain]++;
        }else{
          p.domains[d.domain]=1;
          p.count++;
        }
        return p
      },
      function remove(p, d){
        p.domains[d.domain]--;
        if (p.domains[d.domain] === 0){
          delete p.domains[d.domain];
          p.count--;
        }
        return p
      },
      function ini(d){
        return {count: 0, domains:{}};
      }
    );
    charTotalDomainsCount.dom         
      .valueAccessor( function(d) {return d.value.count; } )       
      .formatNumber(function(v){return changeDecimal(numberFormat(v))})
      .group(charTotalDomainsCount.reduce);
    this._charts['charTotalDomainsCount'] = charTotalDomainsCount;

     // Media de Alerta
    var charAvgAlert = {};
    charAvgAlert.dom = dc.numberDisplay(('#'+this._group+' #avg-alert'),this._group);        
    charAvgAlert.reduce = this._allMap.group().reduce(
      function add(p, d){
        p.count++;
        p.alert+=d.avg_alert
        p.avg = p.alert/p.count
        return p
      },
      function remove(p, d){
        p.count--;
        p.alert-=d.avg_alert
        p.avg = p.alert/p.count
        return p
      },
      function ini(d){
        return {count: 0, alert:0, avg:0};
      }
    );
    charAvgAlert.dom         
      .valueAccessor( function(d) {return d.value.avg; } )       
      .formatNumber(function(v){return changeDecimal(numberFormatDecimal(v))})
      .group(charAvgAlert.reduce);
    this._charts['charAvgAlert'] = charAvgAlert;

    // Valencia media
    var charAvgValence = {};
    charAvgValence.dom = dc.numberDisplay(('#'+this._group+' #avg-valence'),this._group);        
    charAvgValence.reduce = this._allMap.group().reduce(
      function add(p, d){
        p.count++;
        p.valence+=d.avg_valence
        p.avg = p.valence/p.count
        return p
      },
      function remove(p, d){
        p.count--;
        p.valence-=d.avg_valence
        p.avg = p.valence/p.count
        return p
      },
      function ini(d){
        return {count: 0, valence:0, avg:0};
      }
    );
    charAvgValence.dom         
      .valueAccessor( function(d) {return d.value.avg; } )       
      .formatNumber(function(v){return changeDecimal(numberFormatDecimal(v))})
      .group(charAvgValence.reduce);
    this._charts['charAvgValence'] = charAvgValence;




    this.render();
  }

  this.initClouds = function(){
    this._clouds = [];

    // Cloud Terms
    var frequencyTerms = {};
    //Seta os tamanhos
    frequencyTerms.width = $('.cloud-terms').width();
    frequencyTerms.height = 250
    //Cria o a dimensão reduce
    frequencyTerms.reduce = this._allMap.group().reduce(
      function reduceAdd(p, v){
        for(t in v.tokens_count){
          if (t in p){
            p[t]++;
          }else{
            p[t]=1;          
          }                
        }
        return p;
      },
      function reduceRemove(p, v){
        for(t in v.tokens_count){
          p[t]--;
          if(p[t] == 0){
            delete p[t]
          }
        }
        return p;
      },
      function initial(){
        p = {};
        return p;
      }
    );
    //Cria o array de palavras com base no reduce criado    
    frequencyTerms._termsArray = [];
    frequencyTerms.setList = function(){      
      frequencyTerms._termsArray = [];
      var terms = frequencyTerms.reduce.all()[0].value;
      for(t in terms){
        frequencyTerms._termsArray.push({"text":t,"frequency":terms[t]})
      }
      frequencyTerms._termsArray = frequencyTerms._termsArray.sort(function(a,b){
        if (a.frequency > b.frequency)
          return -1
        if (a.frequency < b.frequency)
          return 1
        return 0
      });
      frequencyTerms._termsArray = frequencyTerms._termsArray.slice(0,5);
      var order = 1;      
      for(t in frequencyTerms._termsArray){
        frequencyTerms._termsArray[t].order = order;
        order++;
      }
      minFrequency = frequencyTerms._termsArray[frequencyTerms._termsArray.length-1].frequency;
      maxFrequency = frequencyTerms._termsArray[0].frequency;
      linearScale = d3.scale.linear().domain([minFrequency,maxFrequency]).range([12,37]);
      for(t in frequencyTerms._termsArray){
        frequencyTerms._termsArray[t].size = linearScale(frequencyTerms._termsArray[t].frequency);
      }      
    }    
    //Mapeia o SVG
    // frequencyTerms._sgv = d3.select('.cloud-terms').append("svg")
    //     .attr("width", frequencyTerms.width)
    //     .attr("height", frequencyTerms.height)
    //     .append("g")
    //     .attr("transform", "translate(" + frequencyTerms.width / 2 + "," + frequencyTerms.height / 2 +")");
    frequencyTerms.draw = function(words){

      var color = d3.scale.category20();  
      $('.cloud-terms svg').remove()
      d3.select('.cloud-terms').append("svg")
        .attr("width", frequencyTerms.width)
        .attr("height", frequencyTerms.height)
        .append("g")
          .attr("transform", "translate(" + frequencyTerms.width / 2 + "," + frequencyTerms.height / 2 +")")
        .selectAll("text")
          .data(words, function(d) { return d.text; })
        .enter().append("text")
          .style("font-family", "Impact")
          .style("fill", function(d, i) { 
            return color(i); 
          })
          .attr("text-anchor", "middle")
          .attr('font-size', 1)
          .text(function(d) {
           return d.text; 
          })                  
          .style("font-size", function(d) { return d.size + "px"; })
          .attr("transform", function(d) {
              return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
          })          
          .append("svg:title")
          .text(function(d) {           
            return d.order + 'º ' + d.text + ' : ' + changeDecimal(numberFormat(d.frequency)) + ' resultados (' + changeDecimal(numberFormatDecimal((d.frequency/this._total*100))) + '% )'; 
          }.bind(this));
          
    }.bind(this);
    frequencyTerms.update = function() {
      frequencyTerms.setList();      

      d3.layout.cloud().size([frequencyTerms.width, frequencyTerms.height])
          .words(frequencyTerms._termsArray)
          .padding(1)
          .rotate(function() { return ~~(Math.random() * 2) * 90; })
          .font("Impact")
          .fontSize(function(d) { return d.size; })
          .on("end", frequencyTerms.draw)
          .start();
    }
    this._clouds.push(frequencyTerms);

    //Coud Bigramas    
    var frequencyBigrams = {};
    //Seta os tamanhos
    frequencyBigrams.width = $('.cloud-bigrams').width();
    frequencyBigrams.height = 250
    //Cria o a dimensão reduce
    frequencyBigrams.reduce = this._allMap.group().reduce(
      function reduceAdd(p, v){
        for(t in v.bigrams){
          p[v.bigrams[t]] = (v.bigrams[t] in p ? p[v.bigrams[t]] : 0) + 1;
        }
        return p;
      },
      function reduceRemove(p, v){
        for(t in v.bigrams){
          p[v.bigrams[t]] = (v.bigrams[t] in p ? p[v.bigrams[t]] : 0) - 1;
        }
        return p;
      },
      function initial(){
        p = [];
        return p;
      }
    );
    //Cria o array de palavras com base no reduce criado    
    frequencyBigrams._termsArray = [];
    frequencyBigrams.setList = function(){      
      frequencyBigrams._termsArray = [];
      var terms = frequencyBigrams.reduce.all()[0].value;
      for(t in terms){
        frequencyBigrams._termsArray.push({"text":t,"frequency":terms[t]})
      }
      frequencyBigrams._termsArray = frequencyBigrams._termsArray.sort(function(a,b){
        if (a.frequency > b.frequency)
          return -1
        if (a.frequency < b.frequency)
          return 1
        return 0
      });
      frequencyBigrams._termsArray = frequencyBigrams._termsArray.slice(0,100);
      var order = 1;
      for(t in frequencyBigrams._termsArray){
        frequencyBigrams._termsArray[t].order = order;
        order++;
      }
      minFrequency = frequencyBigrams._termsArray[frequencyBigrams._termsArray.length-1].frequency;
      maxFrequency = frequencyBigrams._termsArray[0].frequency;
      linearScale = d3.scale.linear().domain([minFrequency,maxFrequency]).range([12,37]);
      for(t in frequencyBigrams._termsArray){
        frequencyBigrams._termsArray[t].size = linearScale(frequencyBigrams._termsArray[t].frequency);
      }
    }    
    
    frequencyBigrams.draw = function(words){

      var color = d3.scale.category20();   
      $('.cloud-bigrams svg').remove();
      d3.select('.cloud-bigrams').append("svg")
        .attr("width", frequencyBigrams.width)
        .attr("height", frequencyBigrams.height)
        .append("g")
        .attr("transform", "translate(" + frequencyBigrams.width / 2 + "," + frequencyBigrams.height / 2 +")")
        .selectAll("g text")
          .data(words, function(d) { return d.text; })
        .enter()
          .append("text")
          .style("font-family", "Impact")
          .style("fill", function(d, i) { return color(i); })
          .attr("text-anchor", "middle")
          .attr('font-size', 1)
          .text(function(d) { return d.text; })
          .style("font-size", function(d) { return d.size + "px"; })
          .attr("transform", function(d) {
              return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
          })
          .append("svg:title")
          .text(function(d) {
            return d.order + 'º ' + d.text + ' : ' + changeDecimal(numberFormat(d.frequency)) + ' resultados (' + changeDecimal(numberFormatDecimal((d.frequency/this._total*100))) + '% )'; 
          }.bind(this));
            
    }.bind(this);
    frequencyBigrams.update = function() {
      frequencyBigrams.setList();
      d3.layout.cloud().size([frequencyBigrams.width, frequencyBigrams.height])
          .words(frequencyBigrams._termsArray)
          .padding(1)
          .rotate(function() { return ~~(Math.random() * 2) * 90; })
          .font("Impact")
          .fontSize(function(d) { return d.size; })
          .on("end", frequencyBigrams.draw)
          .start();
    }
    this._clouds.push(frequencyBigrams);

    //Coud Felligns Words
    var frequencyFellign = {};
    //Seta os tamanhos
    frequencyFellign.width = $('.cloud-felligns-words').width();
    frequencyFellign.height = 250
    //Cria o a dimensão reduce
    frequencyFellign.reduce = this._allMap.group().reduce(
      function reduceAdd(p, v){
        for(t in v.word_felligns){
          p[v.word_felligns[t].palavra] = (v.word_felligns[t].palavra in p ? p[v.word_felligns[t].palavra] : 0) + 1;
        }
        return p;
      },
      function reduceRemove(p, v){
        for(t in v.word_felligns[t]){
          p[v.word_felligns[t].palavra] = (v.word_felligns[t].palavra in p ? p[v.word_felligns[t].palavra] : 0) - 1;
        }
        return p;
      },
      function initial(){
        p = [];
        return p;
      }
    );
    //Cria o array de palavras com base no reduce criado    
    frequencyFellign._termsArray = [];
    frequencyFellign.setList = function(){      
      frequencyFellign._termsArray = [];
      var terms = frequencyFellign.reduce.all()[0].value;
      for(t in terms){
        frequencyFellign._termsArray.push({"text":t,"frequency":terms[t]})
      }
      frequencyFellign._termsArray = frequencyFellign._termsArray.sort(function(a,b){
        if (a.frequency > b.frequency)
          return -1
        if (a.frequency < b.frequency)
          return 1
        return 0
      });
      frequencyFellign._termsArray = frequencyFellign._termsArray.slice(0,100);
      var order = 1;
      for(t in frequencyFellign._termsArray){
        frequencyFellign._termsArray[t].order = order;
        order++;
      }
      minFrequency = frequencyFellign._termsArray[frequencyFellign._termsArray.length-1].frequency;
      maxFrequency = frequencyFellign._termsArray[0].frequency;
      linearScale = d3.scale.linear().domain([minFrequency,maxFrequency]).range([12,37]);
      for(t in frequencyFellign._termsArray){
        frequencyFellign._termsArray[t].size = linearScale(frequencyFellign._termsArray[t].frequency);
      }
    }    
    //Mapeia o SVG
    // frequencyFellign._sgv = d3.select('.cloud-felligns-words').append("svg")
    //     .attr("width", frequencyFellign.width)
    //     .attr("height", frequencyFellign.height)
    //     .append("g")
    //     .attr("transform", "translate(" + frequencyFellign.width / 2 + "," + frequencyFellign.height / 2 +")");
    frequencyFellign.draw = function(){

      var color = d3.scale.category20();   
      $('.cloud-felligns-words svg').remove();
      d3.select('.cloud-felligns-words').append("svg")
        .attr("width", frequencyFellign.width)
        .attr("height", frequencyFellign.height)
        .append("g")
        .attr("transform", "translate(" + frequencyFellign.width / 2 + "," + frequencyFellign.height / 2 +")")
      .selectAll("g text")
        .data(frequencyFellign._termsArray, function(d) { return d.text; })
      .enter()
          .append("text")
          .style("font-family", "Impact")
          .style("fill", function(d, i) { return color(i); })
          .attr("text-anchor", "middle")
          .attr('font-size', 1)
          .text(function(d) { return d.text; })
          .style("font-size", function(d) { return d.size + "px"; })
          .attr("transform", function(d) {
              return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
          })
          .append("svg:title")
          .text(function(d) {return d.order + 'º ' + d.text + ' : ' + changeDecimal(numberFormat(d.frequency)) + ' resultados (' + changeDecimal(numberFormatDecimal((d.frequency/this._total*100))) + '% )'; }.bind(this));
                
    }.bind(this)
    frequencyFellign.update = function() {
      frequencyFellign.setList();
      d3.layout.cloud().size([frequencyFellign.width, frequencyFellign.height])
          .words(frequencyFellign._termsArray)
          .padding(1)
          .rotate(function() { return ~~(Math.random() * 2) * 90; })
          .font("Impact")
          .fontSize(function(d) { return d.size; })
          .on("end", frequencyFellign.draw)
          .start();
    }
    this._clouds.push(frequencyFellign);
    

  }
  this.drawClouds = function(){
    for(c in this._clouds){
      this._clouds[c].update()
    }
  }
  this.updateClouds = function(){
    for(c in this._clouds){
      this._clouds[c].update()
    } 
  }
  this.render = function(){
    $('.dashboard-container').show();
    dc.renderAll(this._group);
    this.initClouds();
    this.drawClouds();
    $('.loader-container').hide();
  }
  this.onFiltered = function(){
    this._total = this._allReduceCount.all()[0].value
    this.updateClouds()
  }

};

var dashDilma = new dashboard();

$(document).on('ready',function(){
    dashDilma.init('dashboard-dilma');
});


