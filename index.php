<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Big Duck - WhatTellMe</title>
    <link rel="stylesheet" href="lib/css/bootstrap.min.css">
    <link rel="stylesheet" href="lib/css/dc.min.css">
    <link rel="stylesheet" href="lib/css/app.css">

    <script src="lib/js/jquery-1.12.4.min.js"></script>
    <script src="lib/js/bootstrap.min.js"></script>
    <script src="lib/js/d3.min.js"></script>
    <script src="lib/js/crossfilter.min.js"></script>
    <script src="lib/js/dc.min.js"></script>
    <script src="lib/js/d3.layout.cloud.js"></script>
    <script src="lib/js/app.js"></script>
  </head>
  <body>
    <div class="loader-container container">
      <h1>Big Duck Apps Factory</h1>
      <h3>What Tell Me</h3>
      <p>Carregando...</p>
      <div class="loader"></div>
    </div>
    <div class="dashboard-container">
      <div id="dashboard-dilma">

        <div class="row">

          <div class="col-md-3">
            <div class="panel panel-default">
              <div class="panel-heading"><p class="panel-title">Resultados por Data</p></div>
              <div class="panel-body">
                <div class="chart-date-count dashboard-chart"></div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="panel panel-default">
              <div class="panel-heading"><p class="panel-title titleChart">Resultados por Mês</p></div>
              <div class="panel-body">
                <div class="chart-month-count dashboard-chart">
                  <a class="reset" title="Resetar" href="javascript:dashDilma._charts.charMonthCount.dom.filterAll();dc.redrawAll('dashboard-dilma');" style="display: none;"><span class="glyphicon glyphicon-filter" aria-hidden="true"></span></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="panel panel-default">
              <div class="panel-heading"><p class="panel-title titleChart">Resultados por Site (12 Maiores)</p></div>
              <div class="panel-body">
                <div class="chart-domain-count dashboard-chart">
                  <a class="reset" title="Resetar" href="javascript:dashDilma._charts.charDomainCount.dom.filterAll();dc.redrawAll('dashboard-dilma');" style="display: none;"><span class="glyphicon glyphicon-filter" aria-hidden="true"></span></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="row row-numbers">
              <div class="number-chart col-md-6"><h4>Resultados</h4><p id="total"></p></div>
              <div class="number-chart col-md-6"><h4>Sites</h4><p id="totalDomains"></p></div>
            </div>
            <div class="row row-numbers">
              <div class="number-chart col-md-6"><h4>Alerta Médio</h4><p id="avg-alert"></p></div>
              <div class="number-chart col-md-6"><h4>Valencia Média</h4><p id="avg-valence"></p></div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="panel panel-default">
                  <div class="panel-heading"><p class="panel-title titleChart">Resultados com Sentimentos</p></div>
                  <div class="panel-body">
                    <div class="chart-fellign-count dashboard-chart">
                      <a class="reset" title="Resetar" href="javascript:dashDilma._charts.chartFellignCount.dom.filterAll();dc.redrawAll('dashboard-dilma');" style="display: none;"><span class="glyphicon glyphicon-filter" aria-hidden="true"></span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="row">

          <div class="col-md-3">
            <div class="panel panel-default">
              <div class="panel-heading"><p class="panel-title titleChart">Palavras mais Frequentes</p></div>
              <div class="panel-body">
                <div class="cloud-terms d3-cloud"></div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="panel panel-default">
              <div class="panel-heading"><p class="panel-title titleChart">Bigramas mais Frequentes</p></div>
              <div class="panel-body">
                <div class="cloud-bigrams d3-cloud"></div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="panel panel-default">
              <div class="panel-heading"><p class="panel-title titleChart">Palavras Analisadas mais Frequentes</p></div>
              <div class="panel-body">
                <div class="cloud-felligns-words d3-cloud"></div>
              </div>
            </div>
          </div>

          <div class="col-md-3">

            <div class="row">
              
              <div class="col-md-12">
                <div class="panel panel-default">
                  <div class="panel-heading"><p class="panel-title titleChart">Resultados por Alerta</p></div>
                  <div class="panel-body">
                    <div class="chart-alert-count dashboard-chart">
                      <a class="reset" title="Resetar" href="javascript:dashDilma._charts.charAlertCount.dom.filterAll();dc.redrawAll('dashboard-dilma');" style="display: none;"><span class="glyphicon glyphicon-filter" aria-hidden="true"></span></a>
                    </div>
                  </div>
                </div>  
              </div>

              <div class="col-md-12">
                <div class="panel panel-default">
                  <div class="panel-heading"><p class="panel-title titleChart">Resultados por Valência</p></div>
                  <div class="panel-body">
                    <div class="chart-valence-count dashboard-chart">
                      <a class="reset" title="Resetar" href="javascript:dashDilma._charts.charValenceCount.dom.filterAll();dc.redrawAll('dashboard-dilma');" style="display: none;"><span class="glyphicon glyphicon-filter" aria-hidden="true"></span></a>
                    </div>
                  </div>
                </div>  
              </div>

            </div>

            
          </div>

        </div>

      </div>
    </div>
  </body>
</html>
